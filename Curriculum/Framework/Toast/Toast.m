//
//  Toast.m
//
//  Copyright (c). All rights reserved.
//

#import "Toast.h"

static Toast *shared;

@interface Toast ()


@property (weak, nonatomic) IBOutlet UIImageView *img_campana;
@property (weak, nonatomic) IBOutlet UILabel *lb_mensaje;
@property (strong, nonatomic) NSTimer *timer;

@end

@implementation Toast

#pragma mark - setup

+(Toast *)newInstance
{
    NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"Toast" owner:nil options:nil];
    
    for (UIView *view in arr)
    {
        if ([view isKindOfClass:[Toast class]])
        {
            Toast *obj = (Toast *)view;
            
            CGRect frame = obj.frame;
            frame.size.width = [[UIApplication sharedApplication].delegate window].frame.size.width;
            obj.frame = frame;
            
            return obj;
        }
    }
    
    return nil;
}

+(Toast *)shareInstance
{
    if (!shared)
    {
        shared = [Toast newInstance];
        
        CGRect frame = shared.frame;
        frame.size.width = [[UIApplication sharedApplication].delegate window].frame.size.width;
        shared.frame = frame;
    }
    
    [shared resetView];
    
    return shared;
}

-(void)resetView
{
    _lb_mensaje.text = @"";
}

#pragma mark - Show and Hidde

+(void)toastWithMessage:(NSString *)mensaje
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        Toast *obj = [Toast shareInstance];
        
        [obj stopTimer];
        [obj showWithMessage:mensaje];
        [obj startTimer];
    });
}

-(void)showWithMessage:(NSString *)mensaje
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.lb_mensaje.text = mensaje;
        
        UIWindow *w = [[UIApplication sharedApplication].delegate window];
        
        CGRect frame = self.frame;
        
        frame.origin.y = CGRectGetHeight(w.frame) - CGRectGetHeight(self.frame);
        
        self.frame = frame;
        self.alpha = 0.0;
        
        [self setupPositions];
        
        [w addSubview:self];
        
        [UIView animateWithDuration:0.3 animations:^{
            
            self.alpha = 1.0;
            
        } completion:^(BOOL finished) { }];
    });
}

-(void)hidde
{
    [self stopTimer];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [UIView animateWithDuration:0.3 animations:^{
            
            self.alpha = 0.0;
            
        } completion:^(BOOL finished) {
            
            [self removeFromSuperview];
        }];
    });
}

#pragma mark - Timer

-(void)startTimer
{
    NSLog(@"startTimer");
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(hidde) userInfo:nil repeats:YES];
    //[self.timer fire];
}

-(void)stopTimer
{
    NSLog(@"stopTimer");
    
    [self.timer invalidate];
}

#pragma mark - Setup position

-(void)setupPositions
{
    [_lb_mensaje sizeToFit];
    
    CGRect frameLabel = _lb_mensaje.frame;
    
    frameLabel.origin.x = (CGRectGetWidth(self.frame) - CGRectGetWidth(_lb_mensaje.frame)) / 2.0;
    frameLabel.origin.y = (CGRectGetHeight(self.frame) - CGRectGetHeight(_lb_mensaje.frame)) / 2.0;
    
    _lb_mensaje.frame = frameLabel;
    
    CGRect frameCampna = _img_campana.frame;
    frameCampna.origin.x = CGRectGetMinX(_lb_mensaje.frame) - CGRectGetWidth(_img_campana.frame) - 10.0;
    
    _img_campana.frame = frameCampna;
}

@end
