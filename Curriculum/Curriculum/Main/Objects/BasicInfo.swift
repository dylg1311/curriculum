//
//  BasicInfo.swift
//
//  Created by Diego Yael on 2/13/19.
//  Copyright © 2019 Diego Yael. All rights reserved.
//

import Foundation

let kBasicInfoImgName = "imgName"
let kBasicInfoAddress = "address"
let kBasicInfoName = "name"
let kBasicInfoAge = "age"
let kBasicInfoLanguages = "languages"
let kBasicInfoLevel = "level"
let kBasicInfoResume = "resume"
let kBasicInfoTools = "tools"
let kBasicInfoPublications = "publications"

let kResumeCompany = "company"
let kResumeDesc = "description"
let kResumeDate = "date"
let kResumePosition = "position"
let kResumeAct = "activities"

let kPublicationURL = "url"

enum levelDomain: Float {
    case low = 0.1
    case basic = 0.3
    case intermediate = 0.5
    case advanced = 0.7
    case expert = 1.0
}

class BasicInfo: NSObject
{
    var basicInfoImgName : String!
    var basicInfoAddress : String!
    var basicInfoName : String!
    var basicInfoAge : Int!
    var basicInfoLanguages : [BasicTemplate] = []
    var basicInfoXperience : [Xperience] = []
    var basicInfoTools : [BasicTemplate] = []
    var basicInfoPublications : [Publication] = []

    override init(){ super.init() }
    
    convenience init(dictionary : JSON)
    {
        self.init()
        self.basicInfoImgName = dictionary.parseValue(key: kBasicInfoImgName)
        self.basicInfoAge = dictionary[kBasicInfoAge] as? Int
        self.basicInfoAddress = dictionary.parseValue(key: kBasicInfoAddress)
        self.basicInfoName = dictionary.parseValue(key: kBasicInfoName)
        
        for languageJson in dictionary[kBasicInfoLanguages] as! [JSON]
        {
            self.basicInfoLanguages.append(BasicTemplate.init(dictionary: languageJson))
        }
        
        for xperienceJson in dictionary[kBasicInfoResume] as! [JSON]
        {
            self.basicInfoXperience.append(Xperience.init(dictionary: xperienceJson))
        }
        
        for toolJson in dictionary[kBasicInfoTools] as! [JSON]
        {
            self.basicInfoTools.append(BasicTemplate.init(dictionary: toolJson))
        }
        
        for publicationJson in dictionary[kBasicInfoPublications] as! [JSON]
        {
            self.basicInfoPublications.append(Publication.init(dictionary: publicationJson))
        }
    }
}

class BasicTemplate: NSObject
{
    var basicTemplateName : String!
    var basicTemplateLevel : levelDomain!
    
    override init(){ super.init() }

    convenience init(dictionary : JSON)
    {
        self.init()
        self.basicTemplateName = dictionary.parseValue(key: kBasicInfoName)
        if dictionary.parseValue(key: kBasicInfoLevel) == "intermediate"
        {
            self.basicTemplateLevel = .intermediate
        }else if dictionary.parseValue(key: kBasicInfoLevel) == "basic"
        {
            self.basicTemplateLevel = .basic
        }else if dictionary.parseValue(key: kBasicInfoLevel) == "advanced"
        {
            self.basicTemplateLevel = .advanced
        }else if dictionary.parseValue(key: kBasicInfoLevel) == "expert"
        {
            self.basicTemplateLevel = .expert
        }else
        {
            self.basicTemplateLevel = .low
        }
    }
}

class Xperience: NSObject
{
    var xperienceCompany : String!
    var xperienceDesc : String!
    var xperienceDate : String!
    var xperiencePosition : String!
    var xperienceActivities : String!

    override init(){ super.init() }
    
    convenience init(dictionary : JSON)
    {
        self.init()
        self.xperienceCompany = dictionary.parseValue(key: kResumeCompany)
        self.xperienceDesc = dictionary.parseValue(key: kResumeDesc)
        self.xperienceDate = dictionary.parseValue(key: kResumeDate)
        self.xperiencePosition = dictionary.parseValue(key: kResumePosition)
        self.xperienceActivities = dictionary[kResumeAct] as? String
    }
}


class Publication: NSObject
{
    var publicationName : String!
    var publicationDesc : String!
    var publicationURL : String!
    var publicationImgName : String!
    
    override init(){ super.init() }
    
    convenience init(dictionary : JSON)
    {
        self.init()
        self.publicationName = dictionary.parseValue(key: kBasicInfoName)
        self.publicationDesc = dictionary.parseValue(key: kResumeDesc)
        self.publicationURL = dictionary.parseValue(key: kPublicationURL)
        self.publicationImgName = dictionary.parseValue(key: kBasicInfoImgName)
    }
}
