//
//  WebRecipeVC.swift
//  AmericaMovil
//
//  Created by Diego Yael Luna Gasca on 8/8/19.
//  Copyright © 2019 america. All rights reserved.
//

import Foundation
import WebKit

class WebRecipeVC: UIViewController, WKUIDelegate, WKNavigationDelegate
{
    public var urlOpen : String!
    
    @IBOutlet weak var wk_view: WKWebView!
    @IBOutlet weak var view_loading: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        wk_view.uiDelegate = self
        wk_view.navigationDelegate = self
        let urlRequest = URLRequest(url: URL(string:urlOpen)!)
        wk_view.load(urlRequest)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!)
    {
        self.view_loading.isHidden = true
    }

    @IBAction func click_close(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}
