//
//  MainVC.swift
//  Curriculum
//
//  Created by Diego Yael Luna Gasca on 8/21/19.
//  Copyright © 2019 Diego Yael Luna Gasca. All rights reserved.
//

import Foundation
import UIKit

class MainVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    
    private var currentUrl : String!

    @IBOutlet weak var collection: UICollectionView!
    
    @IBOutlet weak var img_cv: UIImageView!
    
    @IBOutlet weak var lb_name: UILabel!
    @IBOutlet weak var lb_age: UILabel!
    @IBOutlet weak var lb_address: UILabel!
    @IBOutlet weak var lb_language: UILabel!
    
    @IBOutlet weak var pv_level: UIProgressView!
    
    var basicInfo : BasicInfo!
    
    //MARK : - Override methods
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        collection.register(UINib.init(nibName: "PublicationCell", bundle: nil), forCellWithReuseIdentifier: "PublicationCell")

        User.service_getPersonalData(jsonName: "cvjson", onSuccess: { (response) in
            self.basicInfo = response
            DispatchQueue.main.async
            {
                self.lb_name.text = String(format: "Name: %@", self.basicInfo.basicInfoName)
                self.lb_age.text = String(format: "Age: %i", self.basicInfo.basicInfoAge)
                self.lb_address.text = String(format: "Address: %@", self.basicInfo.basicInfoAddress)
                self.img_cv.image = UIImage.init(named: self.basicInfo.basicInfoImgName)
                
                if self.basicInfo.basicInfoLanguages.first != nil
                {
                    self.lb_language.text = String(format: "Language: %@", self.basicInfo.basicInfoLanguages.first!.basicTemplateName)
                    self.pv_level.progress = self.basicInfo.basicInfoLanguages.first!.basicTemplateLevel!.rawValue
                }else
                {
                    self.lb_language.text = "Just native language"
                    self.pv_level.isHidden = true
                }
                
                self.collection.reloadData()
            }
        }) { (error) in
            DispatchQueue.main.async
            {
                Toast.toast(withMessage: error.localizedDescription)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let vc = segue.destination as? CVInt
        {
            vc.basicInfo = self.basicInfo
        }else if let vc = segue.destination as? WebRecipeVC
        {
            vc.urlOpen = currentUrl
        }
    }
    
    //MARK : - UICollectionView methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.basicInfo.basicInfoPublications.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PublicationCell", for: indexPath) as! PublicationCell
        cell.setupWithInfo(publication: self.basicInfo.basicInfoPublications[indexPath.item])
        return cell
    }
 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize.init(width: collectionView.frame.size.height, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        self.currentUrl = self.basicInfo.basicInfoPublications[indexPath.item].publicationURL
        self.performSegue(withIdentifier: "OpenPublicationSegue", sender: nil)
    }
}
