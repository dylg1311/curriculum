//
//  ResumeCell.swift
//  Curriculum
//
//  Created by Diego Yael Luna Gasca on 8/22/19.
//  Copyright © 2019 Diego Yael Luna Gasca. All rights reserved.
//

import Foundation

class ResumeCell: UICollectionViewCell
{
    
    @IBOutlet weak var lb_name: UILabel!
    @IBOutlet weak var lb_date: UILabel!
    @IBOutlet weak var lb_position: UILabel!
    @IBOutlet weak var lb_description: UILabel!
    @IBOutlet weak var lb_activities: UILabel!
    
    func setupWithInfo(resume : Xperience)
    {
        lb_name.text = resume.xperienceCompany
        lb_date.text = resume.xperienceDate
        lb_position.text = resume.xperiencePosition
        lb_description.text = resume.xperienceDesc
        lb_activities.text = resume.xperienceActivities
    }
    
}
