//
//  PublicationCell.swift
//  Curriculum
//
//  Created by Grupo Wayssen on 8/21/19.
//  Copyright © 2019 Diego Yael Luna Gasca. All rights reserved.
//

import Foundation

class PublicationCell: UICollectionViewCell
{
    
    @IBOutlet weak var img_company: UIImageView!
    @IBOutlet weak var lb_desc: UILabel!
    
    
    func setupWithInfo(publication : Publication)
    {
        self.img_company.image = UIImage.init(named: publication.publicationImgName)
        self.lb_desc.text = publication.publicationName
    }
}
