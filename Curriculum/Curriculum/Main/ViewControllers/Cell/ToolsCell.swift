//
//  ToolsCell.swift
//  Curriculum
//
//  Created by Diego Yael Luna Gasca on 8/22/19.
//  Copyright © 2019 Diego Yael Luna Gasca. All rights reserved.
//

import Foundation

class ToolsCell: UICollectionViewCell
{
    
    @IBOutlet weak var lb_name: UILabel!
    @IBOutlet weak var pv_level: UIProgressView!
    
    func setupWithInfo(tool : BasicTemplate)
    {
        lb_name.text = tool.basicTemplateName
        pv_level.progress = tool.basicTemplateLevel!.rawValue
    }
    
}
