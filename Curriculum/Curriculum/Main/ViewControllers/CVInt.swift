//
//  CVInt.swift
//  Curriculum
//
//  Created by Diego Yael Luna Gasca on 8/21/19.
//  Copyright © 2019 Diego Yael Luna Gasca. All rights reserved.
//

import Foundation

class CVInt: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    public var basicInfo : BasicInfo!
    private var currentUrl : String!
    
    @IBOutlet weak var img_cv: UIImageView!
    
    @IBOutlet weak var lb_name: UILabel!
    @IBOutlet weak var lb_age: UILabel!
    @IBOutlet weak var lb_address: UILabel!
    @IBOutlet weak var lb_language: UILabel!
    
    @IBOutlet weak var pv_level: UIProgressView!
    
    @IBOutlet weak var constraint_viewContent: NSLayoutConstraint!
    @IBOutlet weak var constraint_resume: NSLayoutConstraint!
    @IBOutlet weak var collection_resume: UICollectionView!
    @IBOutlet weak var constraint_tool: NSLayoutConstraint!
    @IBOutlet weak var collection_tool: UICollectionView!
    @IBOutlet weak var collection_publications: UICollectionView!
    
    //MARK : - Override methods
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        collection_publications.register(UINib.init(nibName: "PublicationCell", bundle: nil), forCellWithReuseIdentifier: "PublicationCell")
        collection_resume.register(UINib.init(nibName: "ResumeCell", bundle: nil), forCellWithReuseIdentifier: "ResumeCell")
        collection_tool.register(UINib.init(nibName: "ToolsCell", bundle: nil), forCellWithReuseIdentifier: "ToolsCell")
        
        self.lb_name.text = String(format: "Name: %@", self.basicInfo.basicInfoName)
        self.lb_age.text = String(format: "Age: %i", self.basicInfo.basicInfoAge)
        self.lb_address.text = String(format: "Address: %@", self.basicInfo.basicInfoAddress)
        self.img_cv.image = UIImage.init(named: self.basicInfo.basicInfoImgName)
                    
        if self.basicInfo.basicInfoLanguages.first != nil
        {
            self.lb_language.text = String(format: "Language: %@", self.basicInfo.basicInfoLanguages.first!.basicTemplateName)
            self.pv_level.progress = self.basicInfo.basicInfoLanguages.first!.basicTemplateLevel!.rawValue
        }else
        {
            self.lb_language.text = "Just native language"
            self.pv_level.isHidden = true
        }
        
        constraint_resume.constant = CGFloat(self.basicInfo.basicInfoXperience.count * 200) + 15
        constraint_tool.constant = CGFloat(self.basicInfo.basicInfoTools.count * 50) + 30
        constraint_viewContent.constant = constraint_tool.constant + constraint_resume.constant + CGFloat(constraint_viewContent.constant - 170)

    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let vc = segue.destination as? WebRecipeVC
        {
            vc.urlOpen = self.currentUrl
        }
    }
    
    //MARK : - UICollectionView methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == self.collection_tool { return self.basicInfo.basicInfoTools.count }
        else if collectionView == self.collection_resume { return self.basicInfo.basicInfoXperience.count }
        else{ return self.basicInfo.basicInfoPublications.count }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        if collectionView == self.collection_tool
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ToolsCell", for: indexPath) as! ToolsCell
            cell.setupWithInfo(tool: self.basicInfo.basicInfoTools[indexPath.item])
            return cell
        }
        else if collectionView == self.collection_resume
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ResumeCell", for: indexPath) as! ResumeCell
            cell.setupWithInfo(resume: self.basicInfo.basicInfoXperience[indexPath.item])
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PublicationCell", for: indexPath) as! PublicationCell
            cell.setupWithInfo(publication: self.basicInfo.basicInfoPublications[indexPath.item])
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView == self.collection_tool { return CGSize.init(width: collectionView.frame.size.width, height: 50) }
        else if collectionView == self.collection_resume { return CGSize.init(width: collectionView.frame.size.width, height: 200) }
        else { return CGSize.init(width: collectionView.frame.size.height, height: collectionView.frame.size.height) }
    }
 
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == collection_publications
        {
            self.currentUrl = self.basicInfo.basicInfoPublications[indexPath.item].publicationURL
            self.performSegue(withIdentifier: "OpenPublicationSegue", sender: nil)
        }
    }
}
