//
//  User.swift
//
//  Created by Diego Yael on 1/30/19.
//  Copyright © 2019 Diego Yael. All rights reserved.
//

import Foundation

class User: NSObject
{
    
    public class func service_getPersonalData(jsonName : String, onSuccess: @escaping(BasicInfo) -> Void, onError:@escaping(NSError) -> Void)
    {
        let response = self.JSONfromFile(jsonName: jsonName)
        if response.first?.count != 0
        {
            onSuccess(BasicInfo.init(dictionary: (response.first)!))
        }else
        {
            onError(NSError.init(domain: "No fue posible abrir el archivo", code: 100, userInfo: nil))
        }
    }
    
    private class func JSONfromFile(jsonName : String) -> [JSON]
    {
        if let path = Bundle.main.path(forResource: jsonName, ofType: "json")
        {
            do {
                let contents = try String(contentsOfFile: path)
                let data = contents.data(using: .utf8)
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? JSON
                return json?["data"] as! [JSON]
            } catch {
                return [[:]] // contents could not be loaded
            }
        }
        
        return [[:]]
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
  
}
